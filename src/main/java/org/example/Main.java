package org.example;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import  java.util.*;

public class Main {
    public static int id = 0;
    public static int season = 1;
    public static int city = 2;
    public static int date = 3;
    public static int team1 = 4;
    public static int team2 = 5;
    public static int tossWinner = 6;
    public static int tossDecision = 7;
    public static int result = 8;
    public static int dlApplied = 9;

    public static int winner = 10;

    public static int WinByRuns = 11;

    public static int WinByWickets = 12;

    public static int PlayerOfThematch = 13;
    public static int venue = 14;


    public static int matchId = 0;
    public static int inning = 1;
    public static int battingTeam = 2;
    public static int bowlingTeam = 3;
    public static int over = 4;
    public static int ball = 5;
    public static int batsman = 6;
    public static int nonStriker = 7;
    public static int bowler = 8;
    public static int isSuperOver = 9;
    public static int wideRuns = 10;
    public static int byeRuns = 11;
    public static int legByeRuns = 12;
    public static int noBallRuns = 13;
    public  static  int penaltyRuns = 14;
    public static int batsmanRuns = 15;
    public static int extraRuns = 16;
    public static int totalRuns = 17;

    public static void main(String[] args) {
        String MatchesData = "/home/vishal/matches.csv";
        String DeliveriesData = "/home/vishal/deliveries.csv";

        List<Matches>matchesList = ReadMatchesData(MatchesData);
        List<Deliveries>deliveriesList = ReadDeliveriesData(DeliveriesData);

        System.out.println(economyBowlers(matchesList , deliveriesList));
    }

    public static List<Matches> ReadMatchesData(String MatchesData){
        List<Matches> matchData= new ArrayList<>();
        try(BufferedReader br = new BufferedReader(new FileReader(MatchesData))) {
            String heading ;
            boolean headRow = true;
            while ((heading = br.readLine())!= null){
                if(headRow){
                    headRow = false;
                    continue;
                }
                String[] colName = heading.split(",");
                Matches matches = new Matches();
                matches.setId(Integer.parseInt(colName[id]));
                matches.setSeason(Integer.parseInt(colName[season]));
                matches.setCity(colName[city]);
                matches.setDate(colName[date]);
                matches.setTeam1(colName[team1]);
                matches.setTeam2(colName[team2]);
                matches.setTossWinner(colName[tossWinner]);
                matches.setTossDecision(colName[tossDecision]);
                matches.setResult(colName[result]);
                matches.setDlApplied(Integer.parseInt(colName[dlApplied]));
                matches.setWinner(colName[winner]);

                matches.setWinByRuns(Integer.parseInt(colName[WinByRuns]));

                matches.setWinByWickets(Integer.parseInt(colName[WinByWickets]));
                matches.setPlayerOfThematch(colName[PlayerOfThematch]);
                matches.setVenue(colName[venue]);
                matchData.add(matches);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return matchData;
    }

    public  static  List<Deliveries> ReadDeliveriesData(String DeliveriesData)  {
        List<Deliveries> deliveries = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(DeliveriesData))){
            String Heading;
            boolean headRow = true;

            while ((Heading = br.readLine())!= null){
                if (headRow){
                    headRow = false;
                    continue;
                }
                String[] colName = Heading.split(",");

                Deliveries deliveries1 = new Deliveries();
                deliveries1.setMatchId(Integer.parseInt(colName[matchId]));
                deliveries1.setInning(Integer.parseInt(colName[inning]));
                deliveries1.setBattingTeam(colName[battingTeam]);
                deliveries1.setBowlingTeam(colName[bowlingTeam]);
                deliveries1.setOver(Integer.parseInt(colName[over]));
                deliveries1.setBall(Integer.parseInt(colName[ball]));
                deliveries1.setBatsman(colName[batsman]);
                deliveries1.setNonStriker(colName[nonStriker]);
                deliveries1.setBowler(colName[bowler]);
                deliveries1.setIsSuperOver(Integer.parseInt(colName[isSuperOver]));
                deliveries1.setWideRuns(Integer.parseInt(colName[wideRuns]));
                deliveries1.setByeRuns(Integer.parseInt(colName[byeRuns]));
                deliveries1.setLegByeRuns(Integer.parseInt(colName[legByeRuns]));
                deliveries1.setNoBallRuns(Integer.parseInt(colName[noBallRuns]));
                deliveries1.setPenaltyRuns(Integer.parseInt(colName[penaltyRuns]));
                deliveries1.setBatsmanRuns(Integer.parseInt(colName[batsmanRuns]));
                deliveries1.setExtraRuns(Integer.parseInt(colName[extraRuns]));
                deliveries1.setTotalRuns(Integer.parseInt(colName[totalRuns]));
//                deliveries1.setPlayerDismissed(colName[playerDismissed]);
//                deliveries1.setDismissalKind(colName[dismissalKind]);
//                deliveries1.setFielder(colName[fielder]);

                deliveries.add(deliveries1);
            }

        }catch (IOException e){
            e.printStackTrace();
        }
        return deliveries;
    }

    public  static  Map<Integer ,Integer>matchesPerYear (List<Matches>matchesList){
        Map<Integer , Integer>matchesCount = new HashMap<>();
        for (Matches matches : matchesList){
            int season = matches.getSeason();
            matchesCount.put(season , matchesCount.getOrDefault(season , 0)+ 1);
        }
        return  matchesCount;
    }
    public static  Map<String , Integer>matchesWonPerTeam(List<Matches>matchesList){
        Map<String , Integer>matchesWonCount = new HashMap<>();
        for (Matches matches : matchesList){
            String team = matches.getWinner();
            matchesWonCount.put(team , matchesWonCount.getOrDefault(team ,0)+1);
        }
        return  matchesWonCount;
    }

    public  static  Set<Integer>getYears(List<Matches>matchesList , int year){
        Set<Integer>seasonId = new TreeSet<>();
        for (Matches matches:matchesList){
            if (matches.getSeason() == year){
                seasonId.add(matches.getId());
            }

        }
        return  seasonId;
    }

    public  static  Map<String , Integer>extraRunsCounceded(List<Matches>matchesList , List<Deliveries>deliveriesList ){
         int year = 2016;
        Map<String , Integer>extraRuns = new HashMap<>();
        Set<Integer>ids = getYears(matchesList , year);
        for (Deliveries deliveries: deliveriesList){
          int id = deliveries.getMatchId();
          if (ids.contains(id)){
              String team = deliveries.getBowlingTeam();
              int extra_runs = deliveries.getExtraRuns();

              extraRuns.put(team , extraRuns.getOrDefault(team , 0)+ extra_runs);
          }
        }
        return extraRuns;
    }
    public static List<String> economyBowlers(List<Matches>matchesList , List<Deliveries>deliveriesList ){
        Map<String , Integer>runsBybowler = new TreeMap<>();
        Map<String , Integer>ballsBybowler = new HashMap<>();
        int year = 2015;

        Set<Integer>set = getYears(matchesList , year);
        for (Deliveries deliveries: deliveriesList){
            int id = deliveries.getMatchId();
            if(set.contains(id)){
                String bowler = deliveries.getBowler();
                int runs = deliveries.getTotalRuns();
                int balls = 1;
                runsBybowler.put(bowler , runsBybowler.getOrDefault(bowler , 0)+runs);
                ballsBybowler.put(bowler , ballsBybowler.getOrDefault(bowler , 0)+balls);
            }
        }
        Map<String, Double> economyRates = new HashMap<>();
        for (String bowler : runsBybowler.keySet()) {
            int runs = runsBybowler.get(bowler);
            int balls = ballsBybowler.get(bowler);
            double economyRate = (double) runs / balls * 6.0;
            economyRates.put(bowler, economyRate);
        }
        List<Map.Entry<String, Double>> sortedBowlerList = new ArrayList<>(economyRates.entrySet());
        sortedBowlerList.sort(Map.Entry.comparingByValue(Comparator.reverseOrder()));


        List<String> top10BowlerList = new ArrayList<>();
        for (int i = 0; i < Math.min(1, sortedBowlerList.size()); i++) {
            top10BowlerList.add(sortedBowlerList.get(i).getKey());
        }

        return top10BowlerList;


    }

}