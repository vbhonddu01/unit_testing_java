import org.example.Deliveries;
import org.example.Main;
import org.example.Matches;
import org.junit.Test;

import java.util.*;

import static junit.framework.TestCase.assertEquals;
public class TestCase {
    @Test
    public void testNumberOfMatchesPerYear_ExpectedOutput() {
        List<Matches> matches = getTestMatchesData();
        Map<Integer, Integer> actualResult = Main.matchesPerYear(matches);

        assertEquals(Integer.valueOf(2),actualResult.get(2015));
        assertEquals(Integer.valueOf(1),actualResult.get(2016));
    }

    @Test
    public void  testMatchesWonPerTeam(){
        List<Matches>matches = getTestMatchesData();
        Map<String , Integer>result = Main.matchesWonPerTeam(matches);

        assertEquals(Integer.valueOf(2), result.get("Mumbai Indians"));
        assertEquals(Integer.valueOf(1),result.get("Gujarat Lions"));

    }

    @Test
    public void extraRunsCouceded2015(){
        List<Matches>matches= getTestMatchesData();
        List<Deliveries>deliveries = getTestDeliveriesData();
        Map<String , Integer>extraRuns = Main.extraRunsCounceded(matches , deliveries);

        assertEquals(Integer.valueOf(2) , extraRuns.get("Gujarat Lions"));
    }

    @Test
    public void topEconomicalBowler(){
        List<Matches>matches= getTestMatchesData();
        List<Deliveries>deliveries = getTestDeliveriesData();
        List<String> result = Main.economyBowlers(matches , deliveries);

        assertEquals("YS Chahal" ,result);
    }

    private List<Matches> getTestMatchesData() {
        Matches match1 = new Matches();
        match1.setSeason(2015);
        match1.setId(1);
        match1.setWinner("Mumbai Indians");
        match1.setVenue("Eden Gardens");

        Matches match2 = new Matches();
        match2.setSeason(2015);
        match2.setId(2);
        match2.setWinner("Mumbai Indians");
        match2.setVenue("Eden Gardens");

        Matches match3 = new Matches();
        match3.setSeason(2016);
        match3.setId(4);
        match3.setWinner("Gujarat Lions");
        match3.setVenue("Eden Gardens");

        return Arrays.asList(match1, match2, match3);
    }

    private List<Deliveries>getTestDeliveriesData(){
        Deliveries deliveries = new Deliveries();

        deliveries.setMatchId(1);
        deliveries.setBowler("YS Chahal");
        deliveries.setExtraRuns(0);
        deliveries.setTotalRuns(6);
        deliveries.setLegByeRuns(0);
        deliveries.setByeRuns(0);
        deliveries.setPenaltyRuns(0);
        deliveries.setBowlingTeam("Royal Challenger Banglore");


        Deliveries deliveries1 = new Deliveries();
        deliveries1.setMatchId(1);
        deliveries1.setBowler("YS Chahal");
        deliveries1.setTotalRuns(4);
        deliveries1.setExtraRuns(0);
        deliveries1.setLegByeRuns(0);
        deliveries1.setByeRuns(0);
        deliveries1.setPenaltyRuns(0);
        deliveries1.setBowlingTeam("Royal Challenger Banglore");


        Deliveries deliveries2 = new Deliveries();
        deliveries2.setMatchId(2);
        deliveries2.setBowler("YS Chahal");
        deliveries2.setExtraRuns(0);
        deliveries2.setTotalRuns(6);
        deliveries2.setLegByeRuns(0);
        deliveries2.setByeRuns(0);
        deliveries2.setPenaltyRuns(0);
        deliveries2.setBowlingTeam("Royal Challenger Banglore");


        Deliveries deliveries3 = new Deliveries();
        deliveries3.setMatchId(2);
        deliveries3.setBowler("YS Chahal");
        deliveries3.setExtraRuns(0);
        deliveries3.setTotalRuns(4);
        deliveries3.setLegByeRuns(0);
        deliveries3.setByeRuns(0);
        deliveries3.setPenaltyRuns(0);
        deliveries3.setBowlingTeam("Royal Challenger Banglore");


        Deliveries deliveries4 = new Deliveries();
        deliveries4.setMatchId(2);
        deliveries4.setBowler("YS Chahal");
        deliveries4.setExtraRuns(0);
        deliveries4.setTotalRuns(6);
        deliveries4.setLegByeRuns(0);
        deliveries4.setByeRuns(0);
        deliveries4.setPenaltyRuns(0);
        deliveries4.setBowlingTeam("Royal Challenger Banglore");


        Deliveries deliveries5 = new Deliveries();
        deliveries5.setMatchId(2);
        deliveries5.setBowler("YS Chahal");
        deliveries5.setExtraRuns(0);
        deliveries5.setTotalRuns(4);
        deliveries5.setLegByeRuns(0);
        deliveries5.setByeRuns(0);
        deliveries5.setPenaltyRuns(0);
        deliveries5.setBowlingTeam("Royal Challenger Banglore");


        Deliveries deliveries6 = new Deliveries();
        deliveries6.setMatchId(4);
        deliveries6.setBowler("VK Kohli");
        deliveries6.setExtraRuns(2);
        deliveries6.setTotalRuns(2);
        deliveries6.setLegByeRuns(0);
        deliveries6.setByeRuns(0);
        deliveries6.setPenaltyRuns(0);
        deliveries6.setBowlingTeam("Gujarat Lions");

        return Arrays.asList(deliveries ,deliveries1 , deliveries2 , deliveries3 , deliveries4 , deliveries5 , deliveries6);

    }

}
